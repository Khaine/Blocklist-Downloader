import urllib2
import re
import sys, argparse
import subprocess, shlex

#blocklist information

blocklists = {
	'abuse.ch Zeus Tracker (Domain)': {
		'id': 'abusezeusdomain',
		'type': 'list',
		'checks': ['domain'],
		'url':	'https://zeustracker.abuse.ch/blocklist.php?download=baddomains',
		'regex' : '',
		'file' : 'zeus.domain',
		'table' : 'zeus_domain'
	},
	'abuse.ch Zeus Tracker (IP)': {
		'id': 'abusezeusip',
		'type': 'list',
		'checks': ['ip', 'netblock'],
		'url': 'https://zeustracker.abuse.ch/blocklist.php?download=badips',
		'regex' : '',
		'file' : 'zeus.pf',
		'table' : 'zeus'
	},
	'abuse.ch Palevo Tracker (Domain)': {
		'id': 'abusepalevodomain',
		'type': 'list',
		'checks': ['domain'],
		'url':	'https://palevotracker.abuse.ch/blocklists.php?download=domainblocklist',
		'regex' : '',
		'file' : 'palevo.domain',
		'table' : 'palevo_domain'
	},
	'abuse.ch Palevo Tracker (IP)': {
		'id': 'abusepalevoip',
		'type': 'list',
		'checks': ['ip', 'netblock'],
		'regex': '',
		'url':	'https://palevotracker.abuse.ch/blocklists.php?download=ipblocklist',
		'file': 'palevo.pf',
		'table' : 'palevo'
	},
	'malwaredomains.com IP List': {
		'id': 'malwaredomainsip',
		'type': 'list',
		'checks': ['ip', 'netblock'],
		'url': 'http://www.malwaredomainlist.com/hostslist/ip.txt',
		'regex' : '',
		'file' : 'malwaredomains.pf',
		'table' : 'malwaredomains'
	},
	'malwaredomains.com Domain List': {
		'id': 'malwaredomainsdomain',
		'type': 'list',
		'checks': ['domain'],
		'url': 'http://www.malwaredomainlist.com/hostslist/hosts.txt',
		'regex': '',
		'file' : 'malwaredomains.domain',
		'table' : 'malwaredomains_domain'
	},
	'PhishTank': {
		'id': 'phishtank',
		'type': 'list',
		'checks': ['domain'],
		'url': 'http://data.phishtank.com/data/online-valid.csv',
		'regex': '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
		'file' : 'phishtank.domain',
		'table' :'phishtank_domain'

	},
	'malc0de.com List': {
		'id': 'malc0de',
		'type': 'list',
		'checks': ['ip', 'netblock'],
		'url': 'http://malc0de.com/bl/IP_Blacklist.txt',
		'regex' : '',
		'file' : 'malc0de.pf',
		'table' : 'malc0de'
	},
	'TOR Node List': {
		'id': 'tornodes',
		'type': 'list',
		'checks': [ 'ip', 'netblock' ],
		'url': 'http://torstatus.blutmagie.de/ip_list_all.php/Tor_ip_list_ALL.csv',
		'regex' : '',
		'file' : 'tornodes.pf',
		'table' : 'tor_nodes'

	},
	'blocklist.de List': {
		'id': 'blocklistde',
		'type': 'list',
		'checks': [ 'ip', 'netblock' ],
		'url': 'http://lists.blocklist.de/lists/all.txt',
		'regex' : '',
		'file' : 'blocklistde.pf',
		'table' : 'blocklistde'
	},
		'AlienVault IP Reputation Database': {
		'id': 'alienvault',
		'type': 'list',
		'checks': [ 'ip', 'netblock' ],
		'url': 'https://reputation.alienvault.com/reputation.generic',
		'regex': '',
		'file': 'alienvault.pf',
		'table' : 'alienvault'
	},
	'OpenBL.org Blacklist': {
		'id': 'openbl',
		'type': 'list',
		'checks': [ 'ip', 'netblock' ],
		'url': 'http://www.openbl.org/lists/base.txt',
		'regex' : '',
		'file' : 'openbl.pf',
		'table' : 'openbl'
	},
	'Nothink.org SSH Scanners': {
		'id': 'nothinkssh',
		'type': 'list',
		'checks': [ 'ip', 'netblock', 'domain' ],
		'url': 'http://www.nothink.org/blacklist/blacklist_ssh_week.txt',
		'regex' : '',
		'file' : 'nothinkssh.pf',
		'table' : 'nothinkssh'
	},
	'Nothink.org Malware IRC Traffic': {
		'id': 'nothinkirc',
		'type': 'list',
		'checks': [ 'ip', 'netblock', 'domain' ],
		'url': 'http://www.nothink.org/blacklist/blacklist_malware_irc.txt',
		'regex' : '',
		'file' : 'nothinkirc.pf',
		'table' : 'nothinkirc'
	},
	'C.I. Army Malicious IP List': {
		'id': 'ciarmy',
		'type': 'list',
		'checks': [ 'ip', 'netblock' ],
		'url': 'http://cinsscore.com/list/ci-badguys.txt',
		'regex' : '',
		'file' : 'ciarmy.pf',
		'table' : 'ciarmy'
	},
	'Emerging Threats': {
		'id': 'emergingthreats',
		'type': 'list',
		'checks': [ 'ip', 'netblock' ],
		'url': 'http://rules.emergingthreats.net/fwrules/emerging-Block-IPs.txt',
		'regex' : '',
		'file' : 'emergingthreats.pf',
		'table' : 'emergingthreats'
	},
	#'Project Honeypot': {
	#	'id': 'projecthoneypot',
	#	'type': 'list',
	#	'checks': [ 'ip', 'netblock' ],
	#	'url': 'http://www.projecthoneypot.org/list_of_ips.php?t=d&rss=1',
	#	'regex' : '',
	#	'file' : 'projecthoneypot.pf',
	#	'table' : 'projecthoneypot'
	#},
	'Rulez.sk blocklist': {
		'id': 'rulez.sk',
		'type': 'list',
		'checks': [ 'ip', 'netblock' ],
		'url': 'http://danger.rulez.sk/projects/bruteforceblocker/blist.php',
		'regex' : '',
		'file' : 'rulez.sk.pf',
		'table' : 'rulezsk'
	},
	'Firehol blocklist': {
		'id': 'firehol',
		'type': 'list',
		'checks': [ 'ip', 'netblock' ],
		'url': 'https://raw.githubusercontent.com/ktsaou/blocklist-ipsets/master/firehol_level1.netset',
		'regex' : '',
		'file' : 'firehol.pf',
		'table' : 'firehol'
	},
	'Abuse.ch Ransomware': {
			'id': 'ransomware',
			'type': 'list',
			'checks': [ 'ip', 'netblock' ],
			'url': 'https://ransomwaretracker.abuse.ch/downloads/RW_IPBL.txt',
			'regex' : '',
			'file' : 'ransomware.pf',
			'table' : 'ransomware'
	},
	'Binary Defense Systems': {
		'id': 'bindefence',
		'type': 'list',
		'checks': [ 'ip', 'netblock' ],
		'url': 'http://www.binarydefense.com/banlist.txt',
		'regex' : '',
		'file' : 'bindefence.pf',
		'table' : 'bindefence'
	}

}

def downloadAndProcessBlocklist(url, regex, filename):
	req = urllib2.Request(url)
	req.add_header('User-Agent', 'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/5.0)')

	#download blocklist
	try:
		response = urllib2.urlopen(req)
		contents = response.read()

		#process blocklists
		#if regex != '':
	
		#	match = re.findall(regex, contents)
	
		#	print match
	
		#	contents = match

		#remove comments, duplicates and process
		output = re.sub(r'(?m)^\#.*\n?', '', contents)
		#listOutput = re.findall('(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', output)
		listOutput = re.findall('(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)', output)
		
		listOutput = [x for x in listOutput if not x.startswith('// ')]
		listOutput = list(set(listOutput))
					
		#remove whitelist
		listOutput = [x for x in listOutput if x != "127.0.0.1"]
		listOutput = [x for x in listOutput if x != "::1"]
		listOutput = [x for x in listOutput if x != "localhost"]
		listOutput = [x for x in listOutput if x != "s3-eu-west-1.amazonaws.com"]
				
		listOutput = [s+'\n' for s in listOutput]
		
		contents = ''.join(listOutput)

	except urllib2.URLError as e:
		if hasattr(e, 'reason'):
			print 'We failed to reach a server.'
			print 'Reason: ', e.reason
		elif hasattr(e, 'code'):
			print 'The server couldn\'t fulfill the request.'
			print 'Error code: ', e.code
		else:
			print 'unknown error'

	#write to file
	try:
		with open(location+filename, 'w') as f:
			f.write(contents)
			f.close()
	except IOError as e:
			print e.reason

def reloadFirewallRules(firewall, location, table):

	if firewall == 'pf':

		print ('/sbin/pfctl -t ' + table + ' -Tr -f ' + location+value['file'])

		subprocess.check_call(shlex.split('/sbin/pfctl -t ' + table + ' -Tr -f ' + location+value['file']))

#	if firewall == 'iptables':
		#todo

# main

#sensible defaults
firewall = 'pf'
listType = 'ip'
location = '/root/tables/'
blocklist_type = 'ip'

parser = argparse.ArgumentParser(description='IP blocklist downloader and importer for pf and ip tables')
parser.add_argument('-fw', '--firewall_type',help='firewall type, currently pf and iptables are supported', required=False)
parser.add_argument('-t', '--blocklist_type',help='blocklist type, currently ip, netblock, domain and all are supported', required=False)
parser.add_argument('-l', '--blocklist_location',help='location to store blocklists', required=False)
parser.add_argument('-n', '--blocklist_names',help='specify names of blocklists to download', required=False, type=lambda s: [str(item) for item in s.split(',')])

args = parser.parse_args()

if args.blocklist_type in ['ip','domain','netblock']:
	listType = args.blocklist_type
else:
	print('Invalid option, only ip, domain and netblock are currently supported')
	sys.exit(2)

if args.firewall_type != None:
	firewall = args.firewall_type

if args.blocklist_location != None:
	location = args.blocklist_location


for key, value in sorted(blocklists.items()):

	#download all blocklists of the given type
	if args.blocklist_names == None:
		if listType in value['checks']:
			print('downloading '+key)
			downloadAndProcessBlocklist(value['url'], value['regex'], value['file'])
			reloadFirewallRules(firewall, location, value['table'])
	else:
		#download specified blocklists
		if value['id'] in args.blocklist_names:
			print('downloading '+key)
			downloadAndProcessBlocklist(value['url'], value['regex'], value['file'])
			reloadFirewallRules(firewall, location, value['table'])

